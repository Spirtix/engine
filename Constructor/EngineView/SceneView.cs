﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Engine.Components;
using Engine.Input;
using Engine.Renderer;
using System.Drawing;

namespace Constructor.EngineView
{
    class SceneView
    {
        public readonly string name;
        public List<Mesh> meshes = new List<Mesh>();
        Matrix4 projectionMatrix;
        Render rnd = new Render();
        Camera cam = new Camera();
        Keyboard keyboard = new Keyboard();
        Shader shader = ContentProvider.LoadShader("light");
        public static Vector2 lastMousePos = new Vector2();
        public static bool mouseLock = true;

        public SceneView(string name)
        {
            this.name = name;
            //skybox = new Skybox();
            LoadObjects();
        }

        private void LoadObjects()
        {
            try
            {
                using (StreamReader sr = new StreamReader(ContentProvider.scenePath + name + ".scn"))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] data = line.Split('/');
                        Mesh mesh = ContentProvider.LoadModel(data[1]);
                        mesh.name = data[0];
                        mesh.type = data[1];
                        mesh.position = new Vector3(float.Parse(data[2], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[3], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[4], System.Globalization.CultureInfo.InvariantCulture));
                        if (data.Length == 8)
                        {
                            mesh.rotation = new Vector3(float.Parse(data[5], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[6], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[7], System.Globalization.CultureInfo.InvariantCulture));
                        }
                        meshes.Add(mesh);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot load scene" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Environment.Exit(0);
            }
        }

        public void Update(GLControl glc)
        {
            keyboard.HandleCameraMovement(cam);
            projectionMatrix = cam.GetViewMatrix() * Matrix4.CreatePerspectiveFieldOfView(1.3f, glc.ClientSize.Width / (float)glc.ClientSize.Height, 0.01f, 60.0f);
            if (glc.Focused && mouseLock)
            {
                Vector2 delta = lastMousePos - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);

                cam.AddRotation(delta.X, delta.Y);
                ResetCursor(glc);
            }
        }
        public void Draw()
        {
            shader.Use();
            shader.setMat4("projection", projectionMatrix);
            GL.ClearColor(Color4.CornflowerBlue);
            rnd.RenderMeshes(meshes.ToArray(), shader);
            GL.UseProgram(0);
        }

        public void ResetCursor(GLControl window)
        {
            OpenTK.Input.Mouse.SetPosition(Form1.centerLoc.X, Form1.centerLoc.Y);
            lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
        }
    }
}
