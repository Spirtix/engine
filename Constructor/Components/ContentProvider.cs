﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Engine.Renderer;

namespace Engine.Components
{
    static class ContentProvider
    {
        public static string meshPath { get; private set; }
        static string shaderPath;
        public static string texPath { get; private set; }
        static string shaderSuffix;
        public static string scenePath { get; private set; }

        public static void init()
        {
            IniReader ir = new IniReader("props.ini");
            string mPath = ir.Read("Source", "Content");
            shaderPath = mPath + @"\" + ir.Read("Shader", "Content");
            shaderSuffix = ir.Read("ShaderSuffix", "Content");
        }

        public static void SetForProject(string projectPath)
        {
            IniReader ir = new IniReader("props.ini");
            meshPath = projectPath + @"\" + ir.Read("Mesh", "Content");
            texPath = projectPath + @"\" + ir.Read("Texture", "Content");
            scenePath = projectPath + @"\" + ir.Read("Scene", "Content") + @"\";
        }

        public static Mesh LoadModel(string name)
        {
            string mpath = meshPath + @"\" + name;
            Mesh mesh = ObjLoader.loadObj(mpath, name);
            return mesh;
        }
        public static string LoadModelString(string name)
        {
            string mpath = meshPath + @"\" + name;
            return mpath;
        }
        public static Shader LoadShader(string name)
        {
            string fragment = shaderPath + @"\" + name + "f" + "." + shaderSuffix;
            string vertex = shaderPath + @"\" + name + "v" + "." + shaderSuffix;
            return new Shader(vertex, fragment);
        }
        public static Texture LoadTexture(string name)
        {
            string texture = texPath + @"\" + name + "_diff.png";
            return new Texture(texture);
        }
    }
}
