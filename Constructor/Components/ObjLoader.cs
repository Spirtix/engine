﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTK;
using Engine.Renderer;

namespace Engine.Components
{
    class ObjLoader
    {
        public static Mesh loadObj(string path, string name)
        {

            List<Vector3> vertices = new List<Vector3>();
            List<int> faces = new List<int>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> tex = new List<Vector2>();
            List<float> texIdx = new List<float>();
            List<float> normIdx = new List<float>();
            if (!File.Exists(path + ".obj"))
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Unable to open \"" + name + "\", does not exist.");
                Console.ResetColor();
            }

            else
            {
                using (StreamReader sr = new StreamReader(path + ".obj"))
                {
                    while (!sr.EndOfStream)
                    {
                        List<string> text = new List<string>(sr.ReadLine().ToLower().Split(' '));
                        text.RemoveAll(s => s == string.Empty);

                        if (text.Count == 0) { continue; }

                        string type = text[0];
                        text.RemoveAt(0);

                        switch (type)
                        {
                            //vertex
                            case "v":
                                vertices.Add(new Vector3(float.Parse(text[0], System.Globalization.CultureInfo.InvariantCulture), float.Parse(text[1], System.Globalization.CultureInfo.InvariantCulture), float.Parse(text[2], System.Globalization.CultureInfo.InvariantCulture)));
                                break;
                            //normals
                            case "vn":
                                Vector3 vecNorm = new Vector3(float.Parse(text[0], System.Globalization.CultureInfo.InvariantCulture), float.Parse(text[1], System.Globalization.CultureInfo.InvariantCulture), float.Parse(text[2], System.Globalization.CultureInfo.InvariantCulture));
                                normals.Add(vecNorm);
                                break;
                            //texture
                            case "vt":
                                Vector2 vecTex = new Vector2(float.Parse(text[0], System.Globalization.CultureInfo.InvariantCulture), float.Parse(text[1], System.Globalization.CultureInfo.InvariantCulture));
                                tex.Add(vecTex);
                                break;
                            //face
                            case "f":
                                foreach (string w in text)
                                {
                                    if (w.Length == 0)
                                        continue;

                                    string[] comps = w.Split('/');

                                    faces.Add(int.Parse(comps[0]));
                                    if (comps.Length > 1 && comps[1].Length != 0)
                                        texIdx.Add(float.Parse(comps[1], System.Globalization.CultureInfo.InvariantCulture));

                                    if (comps.Length > 2)
                                        normIdx.Add(float.Parse(comps[2], System.Globalization.CultureInfo.InvariantCulture));
                                }
                                break;
                        }
                    }
                }
            }
            List<Vector3> vecVerts = new List<Vector3>();
            List<Vector2> texVerts = new List<Vector2>();
            List<Vector3> normVerts = new List<Vector3>();
            for (int i = 0; i < faces.Count; i++)
            {
                int vertexIndex = faces[i];
                if (texIdx.Count > 0)
                {
                    int textureIndex = int.Parse(texIdx[i].ToString());
                    Vector2 texture = tex[textureIndex - 1];
                    texture.Y = 1f - texture.Y;
                    texVerts.Add(texture);
                }
                int normIndex = int.Parse(normIdx[i].ToString());
                Vector3 vertex = vertices[vertexIndex - 1];
                Vector3 normal = normals[normIndex - 1];
                vecVerts.Add(vertex);

                normVerts.Add(normal);
            }
            string texName = ContentProvider.texPath + @"\" + name + "_diff.png";
            if (File.Exists(texName))
            {
                Console.WriteLine("Loaded " + name + ".obj of total " + vecVerts.Count + " vertices (undindexed, textured)");
                return new Mesh(vecVerts.ToArray(), normVerts.ToArray(), texVerts.ToArray(), name);
            } else
            {
                Console.WriteLine("Loaded " + name + ".obj of total " + vecVerts.Count + " vertices (undindexed)");
                return new Mesh(vecVerts.ToArray(), normVerts.ToArray(), texVerts.ToArray());
            }
        }
    }
}
