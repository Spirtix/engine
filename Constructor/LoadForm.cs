﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Constructor
{
    public partial class LoadForm : Form
    {
        public LoadForm()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            if (!Directory.Exists("res"))
            {
                MessageBox.Show("Core part of Monog Engine is missing!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(10);
            }
            Engine.Components.ContentProvider.init();
            this.Close();
        }
    }
}
