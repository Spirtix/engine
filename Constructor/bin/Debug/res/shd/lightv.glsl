#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 aTexCoords;

out vec3 FragPos;
out vec2 TexCoords;

uniform mat4 projection;
uniform vec3 pos;
uniform mat4 rot;

void main()
{
    FragPos = aPos + pos;
    TexCoords = aTexCoords;
    gl_Position = projection * rot * vec4((FragPos), 1.0);
}