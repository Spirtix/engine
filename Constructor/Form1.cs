﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Engine.Renderer;
using Engine.Components;
using Engine.Input;
using Constructor.EngineView;
using Constructor.ToolForm;
using Constructor.IO;
using System.Drawing;

namespace Constructor
{
    public partial class Form1 : Form
    {
        #region INIT
        Render rnd = new Render();
        public static Point centerLoc;
        public static Matrix4 projectionMatrix;
        SceneView sceneView;
        string projectName, srcPath, binPath, exePath;
        Mesh objSelection;
        public string ProjectName
        {
            set
            {
                projectName = value;
                if (value != null)
                {
                    this.Text = "Monog Constructor - " + value;
                    binPath = projectName + @"\bin";
                    srcPath = projectName + @"\src";
                    exePath = AppDomain.CurrentDomain.BaseDirectory;
                }
            }
        }
        public Form1()
        {
            InitializeComponent();
            InitObjGrid();
            CheckForIllegalCrossThreadCalls = false;
            gbMesh.Hide();
        }

        void InitObjGrid()
        {
            lwObj.Columns.Add("Name", 200);
        }
        #endregion

        #region RENDER
        private void GLV_Load(object sender, EventArgs e)
        {
            tDraw.Enabled = true;
        }

        private void tDraw_Tick(object sender, EventArgs e)
        {
            UpdateFrame();
            RenderFrame();
        }

        private void RenderFrame()
        {
            GL.Enable(EnableCap.DepthTest);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(Color4.CornflowerBlue);
            if (sceneView != null)
                sceneView.Draw();
            GL.Flush();
            GLV.SwapBuffers();
        }

        private void UpdateFrame()
        {
            if (sceneView != null)
                sceneView.Update(GLV);
        }
        #endregion

        #region LOGIC
        void SetupProject()
        {
            StringDialog sd = new StringDialog("New Project", "Enter project name:");
            sd.ShowDialog();
            if (!Directory.Exists(sd.textValue))
            {
                ProjectName = sd.textValue;
                if (projectName != null)
                {
                    Directory.CreateDirectory(projectName);
                    Directory.CreateDirectory(srcPath);
                    ProjectHelper.CopyFilesRecursively(new DirectoryInfo(@"res\engine"), new DirectoryInfo(binPath));
                    ProjectLoaded();
                }
            } else
            {
                MessageBox.Show("Cannot use " + projectName + " as name. Project already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void LoadProject()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = AppDomain.CurrentDomain.BaseDirectory;
            DialogResult dr = fbd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                if (Directory.Exists(fbd.SelectedPath))
                {
                    ProjectName = Path.GetFileName(fbd.SelectedPath);
                    ProjectLoaded();
                }
            }
        }

        void ProjectLoaded()
        {
            ContentProvider.SetForProject(projectName + @"\bin\assets");
            sceneView = new SceneView("test");
            RefreshObjView();
            DirectoryInfo objFoler = new DirectoryInfo(ContentProvider.meshPath);
            foreach (var item in objFoler.GetFiles("*.obj"))
            {
                LoadObject(ContentProvider.LoadModelString(item.FullName));
            }
        }

        void RefreshObjView()
        {
            lwObj.Items.Clear();
            foreach (var item in sceneView.meshes)
            {
                lwObj.Items.Add(item.name);
            }
        }

        void LoadObject(string path)
        {
            try
            {
                string name = path.Split('\\').Last().Split('.').First();
                if (!File.Exists(ContentProvider.meshPath + @"\" + name + ".obj"))
                    File.Copy(path, ContentProvider.meshPath + @"\" + name + ".obj");
                lwAssets.Items.Add(name);
            }
            catch (Exception)
            {
                MessageBox.Show("An error occured while adding an object.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void SetInspector(string obj)
        {
            gbMesh.Show();
            gbMesh.Text = obj;
            objSelection = sceneView.meshes.Find(x => x.name == obj);
            tbPosX.Text = objSelection.position.X.ToString();
            tbPosY.Text = objSelection.position.Y.ToString();
            tbPosZ.Text = objSelection.position.Z.ToString();
            tbRotX.Text = objSelection.rotation.X.ToString();
            tbRotY.Text = objSelection.rotation.Y.ToString();
            tbRotZ.Text = objSelection.rotation.Z.ToString();
        }
        private void ZeroizeTextbox(TextBox tb)
        {
            if (tb.Text == "")
            {
                tb.Text = "0";
                tb.SelectionStart = 0;
                tb.SelectionLength = 1;
            }
        }

        private void Save(string sceneName)
        {
            string sceneFile = ContentProvider.scenePath + sceneName + ".scn";
            if (File.Exists(sceneFile))
                File.Delete(sceneFile);
            using (StreamWriter sw = new StreamWriter(sceneFile))
            {
                foreach (var mesh in sceneView.meshes)
                {
                    string line = mesh.name + "/" + mesh.type + "/" + mesh.position.X + "/" + mesh.position.Y + "/" + mesh.position.Z;
                    if (mesh.rotation != new Vector3(0,0,0))
                        line += "/" + mesh.rotation.X + "/" + mesh.rotation.Y + "/" + mesh.rotation.Z;
                    sw.WriteLine(line);
                }
                sw.Flush();
            }
        }
        #endregion

        #region GUIEvents
        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetupProject();
        }

        private void GLV_Resize(object sender, EventArgs e)
        {
            GL.Viewport(0,0, GLV.ClientSize.Width, GLV.ClientSize.Height);
            centerLoc.X = this.PointToScreen(GLV.Location).X + GLV.Width / 2;
            centerLoc.Y = this.PointToScreen(GLV.Location).Y + GLV.Height / 2;
        }

        private void Form1_Move(object sender, EventArgs e)
        {
            centerLoc.X = this.PointToScreen(GLV.Location).X + GLV.Width / 2;
            centerLoc.Y = this.PointToScreen(GLV.Location).Y + GLV.Height / 2;
        }

        private void lwObj_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lwObj.SelectedItems.Count > 0)
            {
                SetInspector(lwObj.SelectedItems[0].Text);
            } else
            {
                gbMesh.Hide();
            }
        }

        private void lwAssets_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lwAssets.SelectedItems.Count > 0)
            {
                Mesh mesh = ContentProvider.LoadModel(lwAssets.SelectedItems[0].Text);
                mesh.name = lwAssets.SelectedItems[0].Text;
                mesh.type = lwAssets.SelectedItems[0].Text;
                sceneView.meshes.Add(mesh);
                RefreshObjView();
            }
        }

        #region InspectorPosition
        private void tbPosX_TextChanged(object sender, EventArgs e)
        {
            float value;
            if (float.TryParse(tbPosX.Text, out value))
            {
                objSelection.position.X = value;
            }
            ZeroizeTextbox(tbPosX);
        }

        private void tbPosY_TextChanged(object sender, EventArgs e)
        {
            float value;
            if (float.TryParse(tbPosY.Text, out value))
            {
                objSelection.position.Y = value;
            }
            ZeroizeTextbox(tbPosY);
        }

        private void tbPosZ_TextChanged(object sender, EventArgs e)
        {
            float value;
            if (float.TryParse(tbPosZ.Text, out value))
            {
                objSelection.position.Z = value;
            }
            ZeroizeTextbox(tbPosZ);
        }

        private void tbRotX_TextChanged(object sender, EventArgs e)
        {
            float value;
            if (float.TryParse(tbRotX.Text, out value))
            {
                objSelection.rotation = new Vector3(value, objSelection.rotation.Y, objSelection.rotation.Z);
            }
            ZeroizeTextbox(tbRotX);
        }

        private void tbRotY_TextChanged(object sender, EventArgs e)
        {
            float value;
            if (float.TryParse(tbRotY.Text, out value))
            {
                objSelection.rotation = new Vector3(objSelection.rotation.X, value, objSelection.rotation.Z);
            }
            ZeroizeTextbox(tbRotY);
        }

        private void tbRotZ_TextChanged(object sender, EventArgs e)
        {
            float value;
            if (float.TryParse(tbRotZ.Text, out value))
            {
                objSelection.rotation = new Vector3(objSelection.rotation.X, objSelection.rotation.Y, value);
            }
            ZeroizeTextbox(tbRotZ);
        }
        private void saveSceneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save(sceneView.name);
        }
        #endregion

        private void addObjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (projectName != null)
            {
                OpenFileDialog opd = new OpenFileDialog();
                opd.Filter = "Wavefront model|*.obj";
                opd.Title = "Add object";
                opd.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + projectName;

                if (opd.ShowDialog() == DialogResult.OK)
                {
                    if (opd.FileName != null)
                    {
                        LoadObject(opd.FileName);
                    }

                }
            } else
            {
                MessageBox.Show("Cannot add object. Load project first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void loadProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadProject();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                if (projectName != null)
                {
                    try
                    {
                        Save(sceneView.name);
                        System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
                        psi.WorkingDirectory = binPath;
                        psi.FileName = "Engine.exe";
                        System.Diagnostics.Process.Start(psi);
                    }
                    catch
                    {
                        MessageBox.Show("Cannot execute current project", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                } else
                    MessageBox.Show("Cannot execute current project", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
#endregion
    }
}
