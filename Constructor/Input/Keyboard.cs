﻿using System;
using System.Threading;
using OpenTK.Input;
using Engine.Components;
using Constructor.EngineView;
using OpenTK;

namespace Engine.Input
{
    class Keyboard
    {
        KeyboardState keyboardState, lastKeyboardState;
        public bool KeyPress(Key key)
        {
            return (keyboardState[key] && (keyboardState[key] != lastKeyboardState[key]));
        }
        public void HandleCameraMovement(Camera cam)
        {
            keyboardState = OpenTK.Input.Keyboard.GetState();
            if (KeyPress(Key.W))
                cam.Move(0f, 1f, 0f);
            else if (KeyPress(Key.S))
                cam.Move(0f, -1f, 0f);
             if (KeyPress(Key.A))
                cam.Move(-1f, 0f, 0f);
             if (KeyPress(Key.D))
                cam.Move(1f, 0f, 0f);
             if (KeyPress(Key.ShiftLeft))
                cam.Move(0f, 0f, 1f);
             if (KeyPress(Key.LControl))
                cam.Move(0f, 0f, -1f);
            if (KeyPress(Key.L))
                Console.WriteLine(cam.Position);
            if (KeyPress(Key.Escape))
            {
                SceneView.mouseLock = !SceneView.mouseLock;
                System.Threading.Thread.Sleep(100);
                SceneView.lastMousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

            }
        }
    }
}
