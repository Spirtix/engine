﻿namespace Constructor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GLV = new OpenTK.GLControl();
            this.gbObjects = new System.Windows.Forms.GroupBox();
            this.lwObj = new System.Windows.Forms.ListView();
            this.tDraw = new System.Windows.Forms.Timer(this.components);
            this.gbInspect = new System.Windows.Forms.GroupBox();
            this.gbMesh = new System.Windows.Forms.GroupBox();
            this.tbRotZ = new System.Windows.Forms.TextBox();
            this.tbRotY = new System.Windows.Forms.TextBox();
            this.tbRotX = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbPosZ = new System.Windows.Forms.TextBox();
            this.tbPosY = new System.Windows.Forms.TextBox();
            this.tbPosX = new System.Windows.Forms.TextBox();
            this.cmsMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSceneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addObjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbObj = new System.Windows.Forms.GroupBox();
            this.lwAssets = new System.Windows.Forms.ListView();
            this.saveSceneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbObjects.SuspendLayout();
            this.gbInspect.SuspendLayout();
            this.gbMesh.SuspendLayout();
            this.cmsMain.SuspendLayout();
            this.gbObj.SuspendLayout();
            this.SuspendLayout();
            // 
            // GLV
            // 
            this.GLV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GLV.BackColor = System.Drawing.Color.Black;
            this.GLV.Location = new System.Drawing.Point(218, 12);
            this.GLV.Name = "GLV";
            this.GLV.Size = new System.Drawing.Size(589, 428);
            this.GLV.TabIndex = 0;
            this.GLV.VSync = false;
            this.GLV.Load += new System.EventHandler(this.GLV_Load);
            this.GLV.Resize += new System.EventHandler(this.GLV_Resize);
            // 
            // gbObjects
            // 
            this.gbObjects.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbObjects.Controls.Add(this.lwObj);
            this.gbObjects.Location = new System.Drawing.Point(12, 12);
            this.gbObjects.Name = "gbObjects";
            this.gbObjects.Size = new System.Drawing.Size(200, 428);
            this.gbObjects.TabIndex = 2;
            this.gbObjects.TabStop = false;
            this.gbObjects.Text = "Objects";
            // 
            // lwObj
            // 
            this.lwObj.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lwObj.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lwObj.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lwObj.FullRowSelect = true;
            this.lwObj.Location = new System.Drawing.Point(0, 19);
            this.lwObj.Name = "lwObj";
            this.lwObj.Size = new System.Drawing.Size(200, 409);
            this.lwObj.TabIndex = 0;
            this.lwObj.UseCompatibleStateImageBehavior = false;
            this.lwObj.View = System.Windows.Forms.View.Details;
            this.lwObj.SelectedIndexChanged += new System.EventHandler(this.lwObj_SelectedIndexChanged);
            // 
            // tDraw
            // 
            this.tDraw.Interval = 16;
            this.tDraw.Tick += new System.EventHandler(this.tDraw_Tick);
            // 
            // gbInspect
            // 
            this.gbInspect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbInspect.Controls.Add(this.gbMesh);
            this.gbInspect.Location = new System.Drawing.Point(813, 12);
            this.gbInspect.Name = "gbInspect";
            this.gbInspect.Size = new System.Drawing.Size(200, 428);
            this.gbInspect.TabIndex = 1;
            this.gbInspect.TabStop = false;
            this.gbInspect.Text = "Inspect";
            // 
            // gbMesh
            // 
            this.gbMesh.Controls.Add(this.tbRotZ);
            this.gbMesh.Controls.Add(this.tbRotY);
            this.gbMesh.Controls.Add(this.tbRotX);
            this.gbMesh.Controls.Add(this.label2);
            this.gbMesh.Controls.Add(this.label1);
            this.gbMesh.Controls.Add(this.tbPosZ);
            this.gbMesh.Controls.Add(this.tbPosY);
            this.gbMesh.Controls.Add(this.tbPosX);
            this.gbMesh.Location = new System.Drawing.Point(6, 19);
            this.gbMesh.Name = "gbMesh";
            this.gbMesh.Size = new System.Drawing.Size(188, 110);
            this.gbMesh.TabIndex = 0;
            this.gbMesh.TabStop = false;
            // 
            // tbRotZ
            // 
            this.tbRotZ.Location = new System.Drawing.Point(127, 71);
            this.tbRotZ.Name = "tbRotZ";
            this.tbRotZ.Size = new System.Drawing.Size(55, 20);
            this.tbRotZ.TabIndex = 7;
            this.tbRotZ.TextChanged += new System.EventHandler(this.tbRotZ_TextChanged);
            // 
            // tbRotY
            // 
            this.tbRotY.Location = new System.Drawing.Point(66, 71);
            this.tbRotY.Name = "tbRotY";
            this.tbRotY.Size = new System.Drawing.Size(55, 20);
            this.tbRotY.TabIndex = 6;
            this.tbRotY.TextChanged += new System.EventHandler(this.tbRotY_TextChanged);
            // 
            // tbRotX
            // 
            this.tbRotX.Location = new System.Drawing.Point(6, 71);
            this.tbRotX.Name = "tbRotX";
            this.tbRotX.Size = new System.Drawing.Size(55, 20);
            this.tbRotX.TabIndex = 5;
            this.tbRotX.TextChanged += new System.EventHandler(this.tbRotX_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Rotation:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Position:";
            // 
            // tbPosZ
            // 
            this.tbPosZ.Location = new System.Drawing.Point(127, 32);
            this.tbPosZ.Name = "tbPosZ";
            this.tbPosZ.Size = new System.Drawing.Size(55, 20);
            this.tbPosZ.TabIndex = 2;
            this.tbPosZ.TextChanged += new System.EventHandler(this.tbPosZ_TextChanged);
            // 
            // tbPosY
            // 
            this.tbPosY.Location = new System.Drawing.Point(66, 32);
            this.tbPosY.Name = "tbPosY";
            this.tbPosY.Size = new System.Drawing.Size(55, 20);
            this.tbPosY.TabIndex = 1;
            this.tbPosY.TextChanged += new System.EventHandler(this.tbPosY_TextChanged);
            // 
            // tbPosX
            // 
            this.tbPosX.Location = new System.Drawing.Point(6, 32);
            this.tbPosX.Name = "tbPosX";
            this.tbPosX.Size = new System.Drawing.Size(55, 20);
            this.tbPosX.TabIndex = 0;
            this.tbPosX.TextChanged += new System.EventHandler(this.tbPosX_TextChanged);
            // 
            // cmsMain
            // 
            this.cmsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.loadProjectToolStripMenuItem,
            this.newSceneToolStripMenuItem,
            this.saveSceneToolStripMenuItem,
            this.addObjectToolStripMenuItem});
            this.cmsMain.Name = "cmsMain";
            this.cmsMain.Size = new System.Drawing.Size(181, 136);
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newProjectToolStripMenuItem.Text = "New Project";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
            // 
            // loadProjectToolStripMenuItem
            // 
            this.loadProjectToolStripMenuItem.Name = "loadProjectToolStripMenuItem";
            this.loadProjectToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadProjectToolStripMenuItem.Text = "Load Project";
            this.loadProjectToolStripMenuItem.Click += new System.EventHandler(this.loadProjectToolStripMenuItem_Click);
            // 
            // newSceneToolStripMenuItem
            // 
            this.newSceneToolStripMenuItem.Name = "newSceneToolStripMenuItem";
            this.newSceneToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newSceneToolStripMenuItem.Text = "New Scene";
            // 
            // addObjectToolStripMenuItem
            // 
            this.addObjectToolStripMenuItem.Name = "addObjectToolStripMenuItem";
            this.addObjectToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addObjectToolStripMenuItem.Text = "Add Object";
            this.addObjectToolStripMenuItem.Click += new System.EventHandler(this.addObjectToolStripMenuItem_Click);
            // 
            // gbObj
            // 
            this.gbObj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbObj.Controls.Add(this.lwAssets);
            this.gbObj.Location = new System.Drawing.Point(12, 446);
            this.gbObj.Name = "gbObj";
            this.gbObj.Size = new System.Drawing.Size(1001, 103);
            this.gbObj.TabIndex = 3;
            this.gbObj.TabStop = false;
            this.gbObj.Text = "Assets";
            // 
            // lwAssets
            // 
            this.lwAssets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lwAssets.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lwAssets.Location = new System.Drawing.Point(0, 15);
            this.lwAssets.Name = "lwAssets";
            this.lwAssets.Size = new System.Drawing.Size(1001, 88);
            this.lwAssets.TabIndex = 0;
            this.lwAssets.UseCompatibleStateImageBehavior = false;
            this.lwAssets.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lwAssets_MouseDoubleClick);
            // 
            // saveSceneToolStripMenuItem
            // 
            this.saveSceneToolStripMenuItem.Name = "saveSceneToolStripMenuItem";
            this.saveSceneToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveSceneToolStripMenuItem.Text = "Save Scene";
            this.saveSceneToolStripMenuItem.Click += new System.EventHandler(this.saveSceneToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 561);
            this.ContextMenuStrip = this.cmsMain;
            this.Controls.Add(this.gbObj);
            this.Controls.Add(this.gbObjects);
            this.Controls.Add(this.gbInspect);
            this.Controls.Add(this.GLV);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monog Constructor";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.Move += new System.EventHandler(this.Form1_Move);
            this.gbObjects.ResumeLayout(false);
            this.gbInspect.ResumeLayout(false);
            this.gbMesh.ResumeLayout(false);
            this.gbMesh.PerformLayout();
            this.cmsMain.ResumeLayout(false);
            this.gbObj.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private OpenTK.GLControl GLV;
        private System.Windows.Forms.GroupBox gbObjects;
        private System.Windows.Forms.Timer tDraw;
        private System.Windows.Forms.GroupBox gbInspect;
        private System.Windows.Forms.ContextMenuStrip cmsMain;
        private System.Windows.Forms.ToolStripMenuItem newSceneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addObjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadProjectToolStripMenuItem;
        private System.Windows.Forms.ListView lwObj;
        private System.Windows.Forms.GroupBox gbObj;
        private System.Windows.Forms.ListView lwAssets;
        private System.Windows.Forms.GroupBox gbMesh;
        private System.Windows.Forms.TextBox tbPosZ;
        private System.Windows.Forms.TextBox tbPosY;
        private System.Windows.Forms.TextBox tbPosX;
        private System.Windows.Forms.TextBox tbRotZ;
        private System.Windows.Forms.TextBox tbRotY;
        private System.Windows.Forms.TextBox tbRotX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem saveSceneToolStripMenuItem;
    }
}

