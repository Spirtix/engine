﻿using System;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace Engine.Renderer
{
    class Render
    {
        Shader shd;
        public void RenderMeshes(Mesh[] meshes, Shader shader)
        {
            foreach (var m in meshes)
            {
                shd = shader;
                shader.setVec3("pos", m.position);
                shader.setMat4("rot", Matrix4.CreateFromQuaternion(m.rotQuat));
                RenderMesh(m);
            }
        }
        public void RenderMesh(Mesh m)
        {
            shd.setBool("hasTexture", false);
            GL.BindBuffer(BufferTarget.ArrayBuffer, m.VBO);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
            GL.EnableVertexAttribArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, m.NBO);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, 0);
            GL.EnableVertexAttribArray(1);

            if (m.texture != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                shd.setBool("hasTexture", true);
                shd.setInt("diffTex", 0);
                GL.Enable(EnableCap.Texture2D);
                GL.BindTexture(TextureTarget.Texture2D, m.texture.texID);
                GL.BindBuffer(BufferTarget.ArrayBuffer, m.TBO);
                GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, 0, 0);
                GL.EnableVertexAttribArray(2);
            }
            GL.DrawArrays(PrimitiveType.Triangles, 0, m.vertices.Length);
            GL.DisableVertexAttribArray(0);
            GL.DisableVertexAttribArray(1);
            if (m.texture != null)
            {
                GL.DisableVertexAttribArray(2);
            }
        }
    }
}