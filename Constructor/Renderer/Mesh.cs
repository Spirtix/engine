﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Engine.Components;

namespace Engine.Renderer
{
    public class Mesh
    {
        public Vector3[] vertices;
        public Vector3[] normals;
        public Vector2[] textureVertices;
        public Vector3 position;
        public Vector3 rotation
        {
            get
            {
                return rotEuler;
            }
            set
            {
                rotEuler = value;
                Vector3 radians = new Vector3(MathHelper.DegreesToRadians(value.X), MathHelper.DegreesToRadians(value.Y), MathHelper.DegreesToRadians(value.Z));
                rotQuat = Quaternion.FromEulerAngles(radians);
            }
        }
        private Vector3 rotEuler;
        public Quaternion rotQuat;
        public string name;
        public string type;
        public Texture texture;
        public int VBO;
        public int NBO;
        public int TBO;

        public Mesh(Vector3[] vertices, Vector3[] normals, Vector2[] textureVertices, string textureName = null)
        {
            this.vertices = vertices;
            this.normals = normals;
            this.textureVertices = textureVertices;
            position = new Vector3(0, 0, 0);
            rotation = new Vector3(0, 0, 0);
            CreateBuffers();
            if (textureName != null)
                texture = ContentProvider.LoadTexture(textureName);
        }
        public string TextureName
        {
            set
            {
                texture = ContentProvider.LoadTexture(value);
            }
        }
        public void Dispose()
        {
            GL.DeleteBuffer(VBO);
            GL.DeleteBuffer(NBO);
            GL.DeleteBuffer(TBO);
            texture.WipeTexture();
        }
        private void CreateBuffers()
        {
            GL.GenBuffers(1, out VBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * Vector3.SizeInBytes, vertices, BufferUsageHint.StaticDraw);
            GL.GenBuffers(1, out NBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, NBO);
            GL.BufferData(BufferTarget.ArrayBuffer, normals.Length * Vector3.SizeInBytes, normals, BufferUsageHint.StaticDraw);
            GL.GenBuffers(1, out TBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, TBO);
            GL.BufferData(BufferTarget.ArrayBuffer, textureVertices.Length * Vector2.SizeInBytes, textureVertices, BufferUsageHint.StaticDraw);
        }
    }
}
