﻿using System;
using System.Threading;
using OpenTK.Input;
using Engine.Components;
using Engine.Environment;

namespace Engine.Input
{
    class Keyboard
    {
        KeyboardState keyboardState, lastKeyboardState;
        public bool KeyPress(Key key)
        {
            return (keyboardState[key] && (keyboardState[key] != lastKeyboardState[key]));
        }
        public void HandleCameramovement(Camera cam, Scene game)
        {
            keyboardState = OpenTK.Input.Keyboard.GetState();
            if (KeyPress(Key.W))
                cam.Move(0f, 1f, 0f);
            else if (KeyPress(Key.S))
                cam.Move(0f, -1f, 0f);
             if (KeyPress(Key.A))
                cam.Move(-1f, 0f, 0f);
             if (KeyPress(Key.D))
                cam.Move(1f, 0f, 0f);
             if (KeyPress(Key.ShiftLeft))
                cam.Move(0f, 0f, 1f);
             if (KeyPress(Key.LControl))
                cam.Move(0f, 0f, -1f);
             if (KeyPress(Key.P))
                game.DebugCommand();
            if (KeyPress(Key.L))
                Console.WriteLine(cam.Position);
            if (KeyPress(Key.M))
            {
                Window.map = !Window.map;
                Thread.Sleep(100);
            }
             if (KeyPress(Key.X))
            {
                game.mouseLock = !game.mouseLock;
                Thread.Sleep(100);
                if (game.mouseLock == false)
                    cam.MouseSensitivity = 0;
                else
                {
                    cam.MouseSensitivity = 0.005f;
                }
            }
        }
    }
}
