﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Engine.Components;
using Engine.Renderer;
using Engine.Input;
using Engine.Environment;

namespace Engine
{
    public class Window : OpenTK.GameWindow
    {
        Scene scn;
        Shader shd;
        public static bool map = false;
        public static int msaa, shadowRes;
        public static VSyncMode vSync;
        int framebuffer, renderbuffer, msRBO, msFBO, tex, msTex, quadTBO, quadVBO;
        int[] depthMap, depthFBO;
        public bool mouseLock = true;
        float[] quadVertices = {
            -1.0f,  1.0f,
            -1.0f, -1.0f,
            1.0f, -1.0f,
            -1.0f,  1.0f,
            1.0f, -1.0f,
            1.0f,  1.0f};
        float[] quadTex = {
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            1.0f, 1.0f};

        public Window ()
        : base(1280, 720, GraphicsMode.Default, "OpenGL Engine", GameWindowFlags.Default, DisplayDevice.Default, 3, 0, GraphicsContextFlags.Default)
        {
            Console.WriteLine("OpenGL version: " + GL.GetString(StringName.Version));
            VSync = vSync;
        }
        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, this.Width, this.Height);
            UpdateScreenFBO();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            GL.GenBuffers(1, out quadVBO);
            GL.GenBuffers(1, out quadTBO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, quadVBO);
            GL.BufferData(BufferTarget.ArrayBuffer, quadVertices.Length * sizeof(float), quadVertices, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, quadTBO);
            GL.BufferData(BufferTarget.ArrayBuffer, quadTex.Length * sizeof(float), quadTex, BufferUsageHint.StaticDraw);

            CreateScreenFBO();

            scn = new Scene("test");
            CreateDepthBuffer(Scene.lights.Count);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            scn.Update(this);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Lighting);
            GL.Enable(EnableCap.ColorMaterial);
            //Render shadow map
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Front);
            GL.Viewport(0,0, shadowRes, shadowRes);
            for (int i = 0; i < Scene.lights.Count; i++)
            {
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, depthFBO[i]);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                scn.DrawLight(i);
                GL.ActiveTexture(TextureUnit.Texture1 + i);
                GL.BindTexture(TextureTarget.Texture2D, depthMap[i]);
            }
            GL.Disable(EnableCap.CullFace);
            //Draw scene into framebuffer
            GL.BindFramebuffer(FramebufferTarget.Framebuffer,msFBO);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Viewport(0,0, this.Width, this.Height);
            scn.Draw();
            Debug.FPSAnalystics(e);
            //Blit buffers
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, msFBO);
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, framebuffer);
            GL.BlitFramebuffer(0, 0, this.Width, this.Height, 0, 0, this.Width, this.Height, ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Nearest);
            //Draw quad and scene as a texture
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.ClearColor(Color4.CornflowerBlue);
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Disable(EnableCap.DepthTest);
            shd.Use();
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
            GL.Enable(EnableCap.Texture2D);
            GL.BindBuffer(BufferTarget.ArrayBuffer, quadVBO);
            GL.VertexPointer(2, VertexPointerType.Float, 0, 0);
            GL.ActiveTexture(TextureUnit.Texture0);
            //if (map)
            //    GL.BindTexture(TextureTarget.Texture2D, depthMap[0]);
            //else
                GL.BindTexture(TextureTarget.Texture2D, tex);
            GL.BindBuffer(BufferTarget.ArrayBuffer, quadTBO);
            GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, 0);
            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);
            GL.Flush();
            this.SwapBuffers();
        }
        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);

            if (Focused && mouseLock)
            {
                scn.ResetCursor(this);
            }
        }
        private void CreateScreenFBO()
        {
            GL.GenFramebuffers(1, out framebuffer);
            GL.GenTextures(1, out tex);
            GL.GenRenderbuffers(1, out renderbuffer);
            GL.GenFramebuffers(1, out msFBO);
            GL.GenRenderbuffers(1, out msRBO);

            UpdateScreenFBO();
        }
        private void UpdateScreenFBO()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, framebuffer);

            GL.BindTexture(TextureTarget.Texture2D, tex);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, this.Width, this.Height, 0, PixelFormat.Rgb, PixelType.UnsignedByte, (IntPtr)0);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (float)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (float)TextureMinFilter.Nearest);

            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, renderbuffer);
            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, this.Width, this.Height);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, renderbuffer);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, tex, 0);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, msFBO);
            GL.GenTextures(1, out msTex);
            GL.BindTexture(TextureTarget.Texture2DMultisample, msTex);
            GL.TexImage2DMultisample(TextureTargetMultisample.Texture2DMultisample, msaa, PixelInternalFormat.Rgb, this.Width, this.Height, true);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (float)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (float)TextureMinFilter.Nearest);

            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, msRBO);
            GL.RenderbufferStorageMultisample(RenderbufferTarget.Renderbuffer, msaa, RenderbufferStorage.DepthComponent, this.Width, this.Height);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, msRBO);
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, msTex, 0);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            shd = ContentProvider.LoadShader("screen");
        }
        private void CreateDepthBuffer(int numLight)
        {
            depthFBO = new int[numLight];
            depthMap = new int[numLight];
            for (int i = 0; i < numLight; i++)
            {
                GL.GenFramebuffers(1, out depthFBO[i]);
                GL.GenTextures(1, out depthMap[i]);
                GL.BindTexture(TextureTarget.Texture2D, depthMap[i]);
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent, shadowRes, shadowRes, 0, PixelFormat.DepthComponent, PixelType.Float, (IntPtr)null);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (float)TextureMinFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (float)TextureMagFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (float)TextureWrapMode.ClampToBorder);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (float)TextureWrapMode.ClampToBorder);
                float[] borderColor = { 1.0f, 1.0f, 1.0f, 1.0f };
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureBorderColor, borderColor);

                GL.BindTexture(TextureTarget.Texture2D, 0);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, depthFBO[i]);
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, depthMap[i], 0);
                GL.DrawBuffer(DrawBufferMode.None);
                GL.ReadBuffer(ReadBufferMode.None);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            }

        }
    }
}