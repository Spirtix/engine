#version 330 core
out vec3 FragColor;

uniform sampler2D diffTex;
in vec2 TexCoords;

void main()
{
    FragColor = vec3(texture(diffTex, TexCoords));;
} 