#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;
#define NR_SHED 20
out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoords;
out vec4 FragPosLightSpace[NR_SHED];

uniform mat4 projection;
uniform vec3 pos;
uniform mat4 lightSpaceMatrix[NR_SHED];
uniform int cShed;
uniform mat4 rot;

void main()
{
    FragPos = (aPos + pos);
    Normal = aNormal * mat3(transpose(inverse(rot)));
    TexCoords = aTexCoords;
    vec4 temp[NR_SHED];
    for(int i = 0; i < cShed; i++)
        temp[i] = lightSpaceMatrix[i] * rot * vec4(FragPos, 1.0);
    FragPosLightSpace = temp;
    gl_Position = projection * rot * vec4(FragPos, 1.0);
}