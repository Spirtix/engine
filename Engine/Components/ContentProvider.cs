﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Engine.Renderer;

namespace Engine.Components
{
    static class ContentProvider
    {
        static string meshPath;
        static string shaderPath;
        public static string texPath { get; private set; }
        static string shaderSuffix;
        public static string scenePath { get; private set; }

        public static void init()
        {
            IniReader ir = new IniReader("props.ini");
            string mPath = ir.Read("Source", "Content");
            meshPath = mPath + @"\" + ir.Read("Mesh", "Content");
            shaderPath = mPath + @"\" + ir.Read("Shader", "Content");
            texPath = mPath + @"\" + ir.Read("Texture", "Content");
            scenePath = mPath + @"\" + ir.Read("Scene", "Content") + @"\";
            shaderSuffix = ir.Read("ShaderSuffix", "Content");
        }
        public static Model LoadModel(string name)
        {
            string mpath = meshPath + @"\" + name;
            return new Model(mpath, name);
        }
        public static Shader LoadShader(string name)
        {
            string fragment = shaderPath + @"\" + name + "f" + "." + shaderSuffix;
            string vertex = shaderPath + @"\" + name + "v" + "." + shaderSuffix;
            return new Shader(vertex, fragment);
        }
        public static Texture LoadTexture(string name)
        {
            return new Texture(name);
        }
    }
}
