﻿using System;
using OpenTK;

namespace Engine.Components
{
    static class Debug
    {
        static int fps;
        static double totalTime;
        public static void FPSAnalystics(FrameEventArgs e, bool fbfDebug = false)
        {
            if (fbfDebug == true)
                Console.WriteLine("Render loop took: " + e.Time);
            fps++;
            totalTime += e.Time;
            if (totalTime >= 1)
            {
                Console.Title = "Engine - FPS:" + fps;
                fps = 0;
                totalTime = 0;
            }
        }
    }
}
