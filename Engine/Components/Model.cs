﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OpenTK;
using ASMP = Assimp;
using Engine.Renderer;

namespace Engine.Components
{
    class Model
    {
        public List<Mesh> meshes = new List<Mesh>();
        public Vector3 position;
        public Quaternion rotation;
        public string name;
        private List<NodeAnimation> nodeAnimations = new List<NodeAnimation>();

        public Model(string path, string name)
        {
            this.name = name;
            position = new Vector3(0, 0, 0);
            rotation = new Quaternion(0,0,0);
           // rotation = new Quaternion(MathHelper.DegreesToRadians(90), MathHelper.DegreesToRadians(0), MathHelper.DegreesToRadians(0));
            LoadModel(path);
        }

        private void LoadModel(string file)
        {
            ASMP.AssimpContext importer = new ASMP.AssimpContext();
            ASMP.Scene s = importer.ImportFile(file);
            if (s.HasAnimations)
                SetupNodeAnimation(s);
            SetupMeshes(s);
            importer.Dispose();
        }

        private void SetupNodeAnimation(ASMP.Scene scn)
        {
            if (scn.HasAnimations)
            {
                foreach (ASMP.Animation anim in scn.Animations)
                {
                }
            }
        }

        private void SetupMeshes(ASMP.Scene scene)
        {
            for (int i = 0; i < scene.MeshCount; i++)
            {
                ASMP.Node node = scene.RootNode.Children[i];
                ASMP.Vector3D scal;
                ASMP.Vector3D aPos;
                ASMP.Quaternion aRot;
                ASMP.Matrix4x4 mat = node.Transform;
                mat.Decompose(out scal, out aRot, out aPos);
                Vector3 r = ToEulerAngles(new Quaternion(aRot.X, aRot.Y, aRot.Z, aRot.W));
                Vector3 fin = new Vector3(MathHelper.RadiansToDegrees(r.Z), -MathHelper.RadiansToDegrees(r.Y), MathHelper.RadiansToDegrees(r.X));
                Quaternion rot = Quaternion.FromEulerAngles(MathHelper.DegreesToRadians(fin.X), MathHelper.DegreesToRadians(-fin.Y), MathHelper.DegreesToRadians(fin.Z));
                Vector3 pos = new Vector3(aPos.X, aPos.Y, aPos.Z);
                List<Vector3> verts = new List<Vector3>();
                List<Vector3> norms = new List<Vector3>();
                List<Vector2> texs = new List<Vector2>();
                foreach (var v in scene.Meshes[i].Vertices)
                {
                    verts.Add(new Vector3(v.X , v.Y, v.Z));
                }
                foreach (var n in scene.Meshes[i].Normals)
                {
                    norms.Add(new Vector3(n.X, n.Y, n.Z));
                }
                foreach (var t in scene.Meshes[i].TextureCoordinateChannels[0])
                {
                    texs.Add(new Vector2(t.X, 1 - t.Y));
                }
                string texName = ContentProvider.texPath + @"\" + name.Split('.')[0] + "_diff.png";
                Mesh m;
                if (File.Exists(texName))
                {
                    Console.WriteLine("Loaded " + name + " of total " + verts.Count + " vertices (undindexed, textured)");
                    m = new Mesh(verts.ToArray(), norms.ToArray(), texs.ToArray(), texName);
                }
                else
                {
                    Console.WriteLine("Loaded " + name + " of total " + verts.Count + " vertices (undindexed)");
                    m = new Mesh(verts.ToArray(), norms.ToArray(), texs.ToArray());
                }
                m.position = pos;
                m.rotation = rot;
                meshes.Add(m);
            }
        }
        private Vector3 ToEulerAngles(Quaternion q)
        {
            // Store the Euler angles in radians
            Vector3 pitchYawRoll = new Vector3();

            double sqw = q.W * q.W;
            double sqx = q.X * q.X;
            double sqy = q.Y * q.Y;
            double sqz = q.Z * q.Z;

            // If quaternion is normalised the unit is one, otherwise it is the correction factor
            double unit = sqx + sqy + sqz + sqw;
            double test = q.X * q.Y + q.Z * q.W;

            if (test > 0.4999f * unit)                              // 0.4999f OR 0.5f - EPSILON
            {
                // Singularity at north pole
                pitchYawRoll.Y = 2f * (float)Math.Atan2(q.X, q.W);  // Yaw
                pitchYawRoll.X = (float)Math.PI * 0.5f;                         // Pitch
                pitchYawRoll.Z = 0f;                                // Roll
                return pitchYawRoll;
            }
            else if (test < -0.4999f * unit)                        // -0.4999f OR -0.5f + EPSILON
            {
                // Singularity at south pole
                pitchYawRoll.Y = -2f * (float)Math.Atan2(q.X, q.W); // Yaw
                pitchYawRoll.X = (float)-Math.PI * 0.5f;                        // Pitch
                pitchYawRoll.Z = 0f;                                // Roll
                return pitchYawRoll;
            }
            else
            {
                pitchYawRoll.Y = (float)Math.Atan2(2f * q.Y * q.W - 2f * q.X * q.Z, sqx - sqy - sqz + sqw);       // Yaw
                pitchYawRoll.X = (float)Math.Asin(2f * test / unit);                                             // Pitch
                pitchYawRoll.Z = (float)Math.Atan2(2f * q.X * q.W - 2f * q.Y * q.Z, -sqx + sqy - sqz + sqw);      // Roll
            }

            return pitchYawRoll;
        }
    }
}
