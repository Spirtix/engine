﻿using System;
using OpenTK;
namespace Engine
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Components.Settings.EngineSetup();
            new Window().Run(60);
        }
    }
}