﻿using System;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using Engine.Components;

namespace Engine.Renderer
{
    class Render
    {
        public void RenderModels(Model[] models, Shader shader)
        {
            foreach (var m in models)
            {
                Shader shd = shader;
                foreach (var mesh in m.meshes)
                {
                    RenderMesh(mesh, m, shd);
                }
            }
        }
        public void RenderMesh(Mesh m, Model mod, Shader shd)
        {
            shd.setVec3("pos", m.position + mod.position);
            shd.setMat4("rot", Matrix4.CreateFromQuaternion(m.rotation * mod.rotation));
            shd.setBool("hasTexture", false);
            GL.BindBuffer(BufferTarget.ArrayBuffer, m.VBO);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
            GL.EnableVertexAttribArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, m.NBO);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, 0);
            GL.EnableVertexAttribArray(1);
            if (m.texture != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                shd.setBool("hasTexture", true);
                shd.setInt("diffTex", 0);
                GL.Enable(EnableCap.Texture2D);
                GL.BindTexture(TextureTarget.Texture2D, m.texture.texID);
                GL.BindBuffer(BufferTarget.ArrayBuffer, m.TBO);
                GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, 0, 0);
                GL.EnableVertexAttribArray(2);
            }
            GL.DrawArrays(PrimitiveType.Triangles, 0, m.vertices.Length);
            GL.DisableVertexAttribArray(0);
            GL.DisableVertexAttribArray(1);
            if (m.texture != null)
            {
                GL.DisableVertexAttribArray(2);
            }
        }
        public void RenderForLightmap(Model[] models, Shader shader)
        {
            foreach (var mod in models)
            {
                foreach (var m in mod.meshes)
                {
                    shader.setVec3("pos", mod.position + m.position);
                    shader.setMat4("rot", Matrix4.CreateFromQuaternion(m.rotation * mod.rotation));
                    GL.BindBuffer(BufferTarget.ArrayBuffer, m.VBO);
                    GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);
                    GL.EnableVertexAttribArray(0);
                    GL.BindBuffer(BufferTarget.ArrayBuffer, m.NBO);
                    GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 0, 0);
                    GL.EnableVertexAttribArray(1);
                    GL.DrawArrays(PrimitiveType.Triangles, 0, m.vertices.Length);
                    GL.DisableVertexAttribArray(0);
                    GL.DisableVertexAttribArray(1);
                }
            }
        }
    }
}