﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using Engine.Renderer;

namespace Engine.Environment
{
    class Light
    {
        public Matrix4 lightSpaceMatrix { get; set; }
        public Vector3 color;
        public Vector3 position;
        public Vector3 shadowTarget;
        public float range = 40;
        public float intensity = 1;

        public Light(Vector3 position, Vector3 rgbColor)
        {
            this.position = position;
            color = rgbColor;
            lightSpaceMatrix = GenLighSpaceMatrix(position, new Vector3(0,0,0));
        }
        public Light(Vector3 position, Vector3 rgbColor, Vector3 shadowTarget)
        {
            this.position = position;
            color = rgbColor;
            this.shadowTarget = shadowTarget;
            lightSpaceMatrix = GenLighSpaceMatrix(position, shadowTarget);
        }
        private Matrix4 GenLighSpaceMatrix(Vector3 position, Vector3 direction)
        {
            Matrix4 lightProjection = Matrix4.CreateOrthographicOffCenter(-10, 10, -10, 10, 1, 8);
            Matrix4 lightView = Matrix4.LookAt(position, direction, Vector3.UnitY);
            Matrix4 result = lightView * lightProjection;
            return result;
        }
        public static void SetUpLights(Shader shader, List<Light> lights)
        {
            shader.Use();
            shader.setInt("light_lenght", lights.Count);
            shader.setInt("cShed", lights.Count);
            for (int i = 0; i < lights.Count; i++)
            {
                shader.setVec3("pointLight["+i+ "].pos", lights[i].position);
                shader.setVec3("pointLight["+i+ "].col", lights[i].color);
                shader.setFloat("pointLight["+i+ "].range", lights[i].range);
                shader.setFloat("pointLight[" + i + "].intensity", lights[i].intensity);
                shader.setMat4("lightSpaceMatrix["+i+"]", lights[i].lightSpaceMatrix);
                shader.setInt("shadowMap["+i+"]", i + 1);
            }

        }
    }
}
