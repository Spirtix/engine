﻿using System;
using System.Collections.Generic;
using OpenTK;
using Engine.Renderer;
using Engine.Components;

namespace Engine.Environment
{
    class Skybox
    {
        Model skybox;
        Shader skyder = ContentProvider.LoadShader("sky");
        public Skybox()
        {
            skybox = ContentProvider.LoadModel("skybox.obj");
        }
        public void DrawSkybox(Render rnd, Vector3 camPosition)
        {
            skyder.Use();
            skyder.setMat4("projection", Environment.Scene.projectionMatrix);
            skybox.position = camPosition;
            rnd.RenderMesh(skybox.meshes[0], skybox, skyder);
        }
    }
}
