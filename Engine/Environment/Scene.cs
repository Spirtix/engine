﻿using System;
using System.IO;
using System.Collections.Generic;
using OpenTK;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using Engine.Renderer;
using Engine.Components;
using Engine.Input;
using OpenTK.Graphics;

namespace Engine.Environment
{
    class Scene
    {
        readonly string name;
        Skybox skybox;
        public static Matrix4 projectionMatrix;
        public static List<Light> lights = new List<Light>();
        Render rnd = new Render();
        Camera cam = new Camera();
        Keyboard keyboard = new Keyboard();
        static List<Model> models = new List<Model>();
        Vector2 lastMousePos = new Vector2();
        Shader shader = ContentProvider.LoadShader("light");
        Shader depthShader = ContentProvider.LoadShader("depth");
        public bool mouseLock = true;

        public Scene(string name)
        {
            this.name = name;
            skybox = new Skybox();
            lights.Add(new Light(new Vector3(2), new Vector3(1), new Vector3(0,0,0)));
            LoadObjects();
        }

        private void LoadObjects()
        {
            try
            {
                using (StreamReader sr = new StreamReader(ContentProvider.scenePath + name + ".scn"))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] data = line.Split('/');
                        Model model = ContentProvider.LoadModel(data[1]);
                        model.name = data[0];
                        model.position = new Vector3(float.Parse(data[2], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[3], System.Globalization.CultureInfo.InvariantCulture), float.Parse(data[4], System.Globalization.CultureInfo.InvariantCulture));
                        if (data.Length == 8)
                        {
                            Vector3 radians = new Vector3(MathHelper.DegreesToRadians(float.Parse(data[5], System.Globalization.CultureInfo.InvariantCulture)), MathHelper.DegreesToRadians(float.Parse(data[6], System.Globalization.CultureInfo.InvariantCulture)), MathHelper.DegreesToRadians(float.Parse(data[7], System.Globalization.CultureInfo.InvariantCulture)));
                            model.rotation = Quaternion.FromEulerAngles(radians);
                        }
                        models.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != null || ex.Message != "")
                    MessageBox.Show("Cannot load scene: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    MessageBox.Show("Cannot load scene", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Environment.Exit(0);
            }
            GC.Collect();
        }

        public void Update(Window window)
        {
            keyboard.HandleCameramovement(cam, this);
            projectionMatrix = cam.GetViewMatrix() * Matrix4.CreatePerspectiveFieldOfView(1.3f, window.ClientSize.Width / (float)window.ClientSize.Height, 0.01f, 60.0f);
            if (window.Focused)
            {
                Vector2 delta = lastMousePos - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);

                cam.AddRotation(delta.X, delta.Y);
                if (mouseLock)
                    ResetCursor(window);
            }
        }
        public void Draw()
        {
            Light.SetUpLights(shader, lights);
            shader.setVec3("viewPos", cam.Position);
            shader.setMat4("projection", projectionMatrix);
            GL.ClearColor(Color4.CornflowerBlue);
            rnd.RenderModels(models.ToArray(), shader);
            GL.UseProgram(0);
            skybox.DrawSkybox(rnd, cam.Position);
        }

        public void DrawLight(int lightIdnex)
        {
            depthShader.Use();
            depthShader.setMat4("projection", lights[lightIdnex].lightSpaceMatrix);
            Matrix4 tempMat = lights[lightIdnex].lightSpaceMatrix;
            GL.LoadMatrix(ref tempMat);
            GL.ClearColor(Color4.CornflowerBlue);
            rnd.RenderForLightmap(models.ToArray(), depthShader);
        }

        public void ResetCursor(Window window)
        {
            OpenTK.Input.Mouse.SetPosition(window.Bounds.Left + window.Bounds.Width / 2, window.Bounds.Top + window.Bounds.Height / 2);
            lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
        }

        public void DebugCommand()
        {
            bool exit = false;
            Console.WriteLine();
            while (!exit)
            {
                Console.Write("Enter debug command: ");
                string cmd = Console.ReadLine();
                switch (cmd)
                {
                    case "model":
                        Console.Write("Enter model name: ");
                        string name = Console.ReadLine();
                        models.Add(ContentProvider.LoadModel(name));
                        break;
                    case "clearmod":
                        models.Clear();
                        break;
                    case "exit":
                        exit = true;
                        break;
                }
            }
        }
    }

}